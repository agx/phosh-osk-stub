phosh-osk-stub (0.42~rc1-1) unstable; urgency=medium

  * New upstream release
    - Provide layout information so e.g. phosh-mobile-settings can use
      it to show only supported layouts
    - Allow to paste clipboard contents
    - varnam completer: Learn accepted words to improve predictions
  * d/control: B-D on libgmobile-dev
  * Install layout info

 -- Guido Günther <agx@sigxcpu.org>  Fri, 20 Sep 2024 19:37:25 +0200

phosh-osk-stub (0.41.1-1) unstable; urgency=medium

  * New upstream release
    - Unswap Enter and Backspace
    - Require higher velocity to swipe close keyboard

 -- Guido Günther <agx@sigxcpu.org>  Wed, 21 Aug 2024 08:31:36 +0200

phosh-osk-stub (0.41.0-1) unstable; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Wed, 14 Aug 2024 23:11:11 +0200

phosh-osk-stub (0.41.0~rc1-1) unstable; urgency=medium

  * New upstream release
    - Allow to swipe close the keyboard
    - Allow for more rows to support e.g. Thai layouts
    - Add more layouts
    - Allow to open OSK settings from menu popover

 -- Guido Günther <agx@sigxcpu.org>  Thu, 08 Aug 2024 12:41:20 +0200

phosh-osk-stub (0.38.0-1) unstable; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Fri, 05 Apr 2024 16:46:01 +0200

phosh-osk-stub (0.37.0-1) unstable; urgency=medium

  * New upstream release
  * Use signed upstream tarballs

 -- Guido Günther <agx@sigxcpu.org>  Wed, 06 Mar 2024 15:56:47 +0100

phosh-osk-stub (0.36.0-2) unstable; urgency=medium

  * Disable varnam on mips64el
    c-shared mode currently isn't supported on this platform.
    (Closes: #1063246)

 -- Guido Günther <agx@sigxcpu.org>  Tue, 06 Feb 2024 15:28:10 +0100

phosh-osk-stub (0.36.0-1) unstable; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Sat, 03 Feb 2024 12:10:28 +0100

phosh-osk-stub (0.35.0-2) unstable; urgency=medium

  * Enable varnam support

 -- Guido Günther <agx@sigxcpu.org>  Sun, 07 Jan 2024 10:27:14 +0100

phosh-osk-stub (0.35.0-1) unstable; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Thu, 04 Jan 2024 20:00:27 +0100

phosh-osk-stub (0.34.0-1) unstable; urgency=medium

  * New upstream release
  * Drop patches. Applied upstream.

 -- Guido Günther <agx@sigxcpu.org>  Mon, 04 Dec 2023 18:14:50 +0100

phosh-osk-stub (0.33.0-1) unstable; urgency=medium

  * New upstream release
  * d/rules: Print details on test failures
  * tests: Use memory gsettings backend

 -- Guido Günther <agx@sigxcpu.org>  Fri, 03 Nov 2023 11:12:10 +0100

phosh-osk-stub (0.32.0-1) unstable; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Tue, 03 Oct 2023 14:12:27 +0200

phosh-osk-stub (0.31.0-1) unstable; urgency=medium

  * Upload to unstable to make it easier to grab
  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Mon, 04 Sep 2023 12:05:44 +0200

phosh-osk-stub (0.30.0-1) experimental; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Wed, 02 Aug 2023 12:04:13 +0200

phosh-osk-stub (0.28.0-1) experimental; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Thu, 01 Jun 2023 10:04:59 +0200

phosh-osk-stub (0.27.0-1) experimental; urgency=medium

  * New upstream release
  * d/control: Add hunspell dependencies
  * d/install: Install metainfo

 -- Guido Günther <agx@sigxcpu.org>  Sun, 30 Apr 2023 17:44:26 +0200

phosh-osk-stub (0.25.0-1) experimental; urgency=medium

  * New upstream release

 -- Guido Günther <agx@sigxcpu.org>  Tue, 28 Feb 2023 10:58:54 +0100

phosh-osk-stub (0.24.0-1) experimental; urgency=medium

  * New upstream release
  * Run tests under xvfb.
    They use gtk_test_init() now which needs a display.
  * Move desktop file out of /usr/share/applications.
    This avoids double listing them in phosh-mobile-settings.
  * Bump standards version

 -- Guido Günther <agx@sigxcpu.org>  Tue, 31 Jan 2023 13:07:22 +0100

phosh-osk-stub (0.23.0~rc2-1) experimental; urgency=medium

  * New upstream release
  * Install manpage
  * Install settings schema.
    Needed for the shortcuts
  * Drop all patches
  * Add symlinks for presage's en dictionary.
    The training corups used for it isn't nice but it's a start
    until we can train from some better dat.
  * fixup! Install manpage
  * d/control: Add deps and breaks for completion

 -- Guido Günther <agx@sigxcpu.org>  Tue, 27 Dec 2022 18:37:15 +0100

phosh-osk-stub (0.23.0~rc1-2) experimental; urgency=medium

  * Allow to use non standard desktop file name.  This avoids dh-exec working
    round #831786 making the arch all build pass.

 -- Guido Günther <agx@sigxcpu.org>  Fri, 09 Dec 2022 20:43:22 +0100

phosh-osk-stub (0.23.0~rc1-1) experimental; urgency=medium

  * Initial Debian release (Closes: #1025060)

 -- Guido Günther <agx@sigxcpu.org>  Tue, 29 Nov 2022 17:00:57 +0100
